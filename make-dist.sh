#!/bin/sh -ex

NAME=xkeycaps
VERSION=$(dpkg-parsechangelog 2>&1 | perl -ne 'print $1 if /^Version: ([^-]*)/')
TGZ=${NAME}_$VERSION.orig.tar.gz
TGZ_DIR=$NAME-$VERSION

[ ! -f ../$TGZ ]
mkdir ../$TGZ_DIR
cp -a . ../$TGZ_DIR
rm -rf ../$TGZ_DIR/debian
find ../$TGZ_DIR -name .svn | xargs rm -rf
cd .. && tar cvz -f $TGZ $TGZ_DIR
rm -rf ../$TGZ_DIR

