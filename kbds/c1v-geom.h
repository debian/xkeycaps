/* xkeycaps, Copyright (c) 1991, 1992, 1993, 1997
 * Jamie Zawinski <jwz@jwz.org>
 *
 * This file describes the physical layout of the Sony PCG-C1V ("Picturebook")
 * series keyboard. (The keycaps in the comments describe the German version.)
 * by Christoph Berg <cb@cs.uni-sb.de> 010901, 030112
 * (copied from pc105-geom.h)
 *
 * Still missing, but not important: keys and caps for the numeric keypad. (cb)
 */

static const struct key_geometry c1v_geom_row0 [] = {
 {6, 5}, /* ESC */
 {6, 5}, {5, 5}, {6, 5}, {6, 5}, /* F (F2 and F8 smaller to make it fit) */
 {6, 5}, {6, 5}, {6, 5}, {5, 5},
 {6, 5}, {6, 5}, {6, 5}, {6, 5},
 {6, 5}, {6, 5}, {6, 5}, {6, 5}, /* misc */
 {3, 0}, {6, 5}, {2, 0}, {6, 5}, {6, 5} /* Fn */
};

static const struct key_geometry c1v_geom_row1 [] = {
 {10, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7},
 {7, 7}, {7, 7}, {7, 7},
 {9, 7}, {11, 7} /* ', Backspace */
};

static const struct key_geometry c1v_geom_row2 [] = {
 {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7},
 {7, 7}, {7, 7}, {7, 7}, {7, 7}, {2, 0}, {7, 14}
};

static const struct key_geometry c1v_geom_row3 [] = {
 {9, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7},
 {7, 7}, {7, 7}, {7, 7}, {7, 7}
};

static const struct key_geometry c1v_geom_row4 [] = {
 {7, 7}, {6, 7}, /* Shift, < (should be {5, 7} but this looks ugly) */
 {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7}, {7, 7},
 {5, 7}, {5, 7}, {7, 7}, {7, 7}, /* -, Fn, Up, Shift */
 {11, 0}, {7, 7} /* (space), Fn: PgUp */
};

static const struct key_geometry c1v_geom_row5 [] = {
 {7, 7}, {7, 7}, {5, 7}, {5, 7}, /* Strg, Fn, ^, Win */
 {5, 7}, {33, 7}, {5, 7}, {5, 7}, /* Alt, Space, AltGr, Menu */
 {7, 7}, {7, 7}, {7, 7}, {7, 7}, /* Strg, Left, Down, Right */
 {4, 0}, {7, 7}, {7, 7}, {7, 7} /* (space), Fn: Home, PgDn, End */
};

static const struct keyboard_row_geometry c1v_geom_rows [] = {
  ROW (c1v_geom_row0, 5), /* the top row is smaller */
  ROW (c1v_geom_row1, 7),
  ROW (c1v_geom_row2, 7),
  ROW (c1v_geom_row3, 7),
  ROW (c1v_geom_row4, 7),
  ROW (c1v_geom_row5, 7),
};

static const struct keyboard_geometry c1v_geom = {
  sizeof (c1v_geom_rows) / sizeof (struct keyboard_row_geometry),
  c1v_geom_rows, 6, 3, 3
};
