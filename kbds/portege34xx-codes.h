/* xkeycaps, Copyright (c) 1998 Jamie Zawinski <jwz@jwz.org>
 *
 * This file describes the keycodes of a Toshiba Portege 34xxCT laptop.
 * By Antony Pavloff <antony@pc77a.cs.msu.su>
 * Used code for Tecra 500CDT by Cesar Augusto Rorato Crusius.
 */

static const KeyCode portege34xx_codes [] = {

  /* Row 0 */
  9, 
  67, 68, 69, 70,
  71, 72, 73, 74,
  75, 76, 95, 96,
  111, 110, 115, 117,

  /* Row 2 */
  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 97,

  /* Row 3 */
  23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 51, 99,

  /* Row 4 */
  66, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 36, 105,

  /* Row 5 */
  50, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62,
  98, 103,

  /* Row 6 */
  37, 0, 64, 65, 49, 106, 107, 100, 104, 102
};
