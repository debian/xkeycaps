/* xkeycaps, Copyright (c) 1998 Jamie Zawinski <jwz@jwz.org>
 *
 * This file describes the physical layout of the Toshiba Portege 34xxCT
 * laptop, and may be suitable for many others 
 * By Antony Pavloff <antony@pc77a.cs.msu.su>
 * Used code for Tecra 500CDT by Cesar Augusto Rorato Crusius.
 */

static const struct key_geometry portege34xx_geom_row0 [] = {
    {16, 10}, {16, 10}, {16, 10}, {16, 10}, {16, 10}, {16, 10},
    {16, 10}, {16, 10}, {16, 10}, {16, 10}, {16, 10}, {16, 10},
    {16, 10}, {16, 10}, {16, 10}, {16, 10}, {16, 10}};

static const struct key_geometry portege34xx_geom_row2 [] = {
    {7,0}, {18,18}, {18,18}, {18,18}, {18,18}, {18,18},
    {18,18}, {18,18}, {18,18}, {18,18}, {18,18}, {18,18},
    {18,18}, {31,18}, {18,18}};

static const struct key_geometry portege34xx_geom_row3 [] = {
    {16,18}, {18,18}, {18,18}, {18,18}, {18,18}, {18,18},
    {18,18}, {18,18}, {18,18}, {18,18}, {18,18}, {18,18},
    {18,18}, {22,18}, {18,18}};

static const struct key_geometry portege34xx_geom_row4 [] = {
    {19,18}, {18,18}, {18,18}, {18,18}, {18,18}, {18,18},
    {18,18}, {18,18}, {18,18}, {18,18}, {18,18}, {18,18}, {37,18},
    {18,18}};

static const struct key_geometry portege34xx_geom_row5 [] = {
    {29,18}, {18,18}, {18,18}, {18,18}, {18,18}, {18,18}, {18,18},
    {18,18}, {18,18}, {18,18}, {18,18}, {27,18}, {18,18},
    {18,18}};

static const struct key_geometry portege34xx_geom_row6 [] = {
    {21,18}, {19,18}, {19,18}, {105,18}, {18,18}, {18,18},
    {18,18}, {18,18}, {18,18}, {18,18}};

static const struct keyboard_row_geometry portege34xx_geom_rows [] = {
    ROW (portege34xx_geom_row0, 18),
    ROW (portege34xx_geom_row2, 18),
    ROW (portege34xx_geom_row3, 18),
    ROW (portege34xx_geom_row4, 18),
    ROW (portege34xx_geom_row5, 18),
    ROW (portege34xx_geom_row6, 18),
};

static const struct keyboard_geometry portege34xx_geom = {
  sizeof (portege34xx_geom_rows) / sizeof (struct keyboard_row_geometry),
  portege34xx_geom_rows, 2, 3, 3
};
